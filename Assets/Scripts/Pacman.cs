﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pacman : MonoBehaviour
{
    Rigidbody body;
    public int step;
    GameObject player;
    Vector3 temp = new Vector3(428.4f, 20.7f, 497.7f);

    public bool invulnerability;
    void Start()
    {
        body = GetComponent<Rigidbody>();
        player = GameObject.FindGameObjectWithTag("Player");
        invulnerability = false;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            body.AddForce(-step, 0, 0);
            //player.transform.Rotate(0, 270, 0);
        }

        if (Input.GetKey(KeyCode.S))
        {
            body.AddForce(step, 0, 0);
            //player.transform.Rotate(0, 90, 0);
        }

        if (Input.GetKey(KeyCode.A))
        {
            body.AddForce(0, 0, -step);
            //player.transform.Rotate(0, 180, 0);
        }

        if (Input.GetKey(KeyCode.D))
        {
            body.AddForce(0, 0, step);
            //player.transform.Rotate(0, 0, 0);
        }

        if (transform.position.y < 0)
        {
            player.transform.position = temp;
        }
    }

    public void EatPill()
    {
        invulnerability = true;
    }

    public bool GetInvulnerability()
    {
        return invulnerability;
    }
}
