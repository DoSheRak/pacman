﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScorePanel : MonoBehaviour
{
    public Text scoreText;

    void Start()
    {
        scoreText.text = FindObjectOfType<GameManager>().GetResult().ToString();
    }

    void Update()
    {
        
    }
}
