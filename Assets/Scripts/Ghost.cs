﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ghost : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (FindObjectOfType<Pacman>().invulnerability == false)
            {
                if (FindObjectOfType<GameManager>().firstLive.enabled != false)
                {
                    FindObjectOfType<GameManager>().KillPacmanFirst();
                }
                else if (FindObjectOfType<GameManager>().secondLive.enabled != false)
                {
                    FindObjectOfType<GameManager>().KillPacmanSecond();
                }
                else if (FindObjectOfType<GameManager>().thirdLive.enabled != false)
                {
                    FindObjectOfType<GameManager>().KillPacmanThird();
                }
            }
            else
            {
                FindObjectOfType<GameManager>().AddGhost(100);
                Destroy(gameObject);
            }
        }
    }


    GameObject player;
    NavMeshAgent navigator;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        navigator = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        navigator.destination = player.transform.position;
    }
}
