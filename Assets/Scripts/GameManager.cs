﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int result;
    public Text scoreText;
    public int lives = 3;
    public Image firstLive;
    public Image secondLive;
    public Image thirdLive;
    GameObject player;
    public float invulnerabilityDrain = 5f;
    float invulnerabilityTime = 60;
    public float currentInvulnerability;

    public void AddCoin(int coin)
    {
        result += coin;
    }

    public void AddPill(int pill)
    {
        result += pill;
        FindObjectOfType<Pacman>().EatPill();
        currentInvulnerability = invulnerabilityTime;
    }

    public void AddGhost(int ghost)
    {
        result += ghost;
    }

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        result = 0;
    }

    void Update()
    {
        scoreText.text = result.ToString();

        if (lives <= 0)
        {
            SceneManager.LoadScene("GameOver");
        }

        if (result >= 1600)
        {
            SceneManager.LoadScene("Victory");
        }

        if (FindObjectOfType<Pacman>().GetInvulnerability())
        {
            currentInvulnerability -= invulnerabilityDrain * Time.deltaTime;
        }

        if (currentInvulnerability < 0)
        {
            FindObjectOfType<Pacman>().invulnerability = false;
        }
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void KillPacmanFirst()
    {
        firstLive.enabled = false;
        lives--;
    }
    
    public void KillPacmanSecond()
    {
        secondLive.enabled = false;
        lives--;
    }
    
    public void KillPacmanThird()
    {
        thirdLive.enabled = false;
        lives--;
    }

    public int GetResult()
    {
        return result;
    }
}
